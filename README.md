# Shopify Tag Creator CLI version

[![N|Solid](https://i.imgur.com/emN1wme.png)](http://fleone.pythonanywhere.com)

Shopify tag creator is a web based text pattern recognition that allows you to easily upload your product CSV export and a json file containing all the keywords to obtain in output a product export with the tag matches in your tag column.  

### Installation

Clone the repository and create the virtual environment

```sh
$ git clone https://gitlab.com/fleonen/shopify-tag-generator.git
$ cd shopify-tag-generator/
$ python -m venv .
$ pip install -r requirements.txt 
$ source /bin/activate
```


### Packages used

Shopify tag creator uses the following python packages:

| Package | Docs |
| ------ | ------ |
| Pandas | [Pandas](https://pandas.pydata.org/) |
| Json | [Json](https://docs.python.org/3/library/json.html) |
| Re | [Regex](https://docs.python.org/3/library/re.html) |
| Os | [OS](https://docs.python.org/3/library/os.html) |


### Development

Want to contribute? Great!

Follow the installation guide and submit a pull request! You will find in the repo comments with ```@Todo``` this is priority, but feel free to suggest what you feel is appropriate!
