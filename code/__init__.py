import pandas as pd
from os import getcwd
import re
import json
#from pprint import pprint 

def setDataFrameOptions():
    """
    Setting up the dataframe options.
    """
    try:
        pd.set_option("display.max_colwidth", 10000)
    except:
        return ('Failed to set your dataframe preferences.')

def ReadCSV(filename):
    """
    Returning the dataframe object from the resource folder.
    """
    try:
        return pd.read_csv(getcwd() + '\\resources\\' + filename, encoding='utf-8', na_filter=False)
    except:
        return ('Could not find the specified filename.')

def GenerateAccuracyReport():
    pass

def ExperimentalApply(dataframe):
    """
    This method will be applied to all the cells of the dataframe and it will look for the same information as SetKeys but it will output what
    has been found in the document itself, useful scenario: I've found in the dataframe 'bath' 6 times in the fields so I am pretty confident 
    assigning a 'bath' tag with a good level of accuracy.
    This generated file will be used to avoid some tags misplacement and give a certain % of accuracy for that tag.
    """
    try:
        with open(getcwd() + '\\resources\\keywords.json') as kwordsfile:
            keywords = json.load(kwordsfile)
    except:
        return ('Could not load your keywords.')

    return ','.join([tag for tag,keyword in keywords.items() if (re.compile('(?i).*{}.*'.format(keyword))).match(dataframe)])

def SetKeys(dataframe):
    """
    Method that performs a regex check in the body and title of the dataframe
    allowing a keyword_holder list to be populated.
    @todo: avoid forlooping each row of the dataframe and think about a more elegant and performing solution.
    """
    # Tag that will be written in the 'Tags' column, keyword to look for.
    try:
        with open(getcwd() + '\\resources\\keywords.json') as kwordsfile:
            keywords = json.load(kwordsfile)
    except:
        return ('Could not load your keywords.')
	
    dataframe = dataframe.applymap(str) # Applymap to convert everything to string
    for index,row in dataframe.iterrows():
        keyword_holder = [] # Empty each row the list containing the keywords          
        try:
            if float(row['Variant Price']) > 0 and float(row['Variant Price']) < 16:
                keyword_holder.append('Price_€0 - €15')
            elif float(row['Variant Price']) > 15 and float(row['Variant Price']) < 26:
                keyword_holder.append('Price_€15 - €25')
            elif float(row['Variant Price']) > 25 and float(row['Variant Price']) < 36:
                keyword_holder.append('Price_€25 - €35')
            elif float(row['Variant Price']) > 35 and float(row['Variant Price']) < 51:
                keyword_holder.append('Price_€35 - €50')
            elif float(row['Variant Price']) > 50 and float(row['Variant Price']) < 76:
                keyword_holder.append('Price_€50 - €75')
        except:
            pass # No op to avoid missing float values    
        for tag, keyword in keywords.items():
            pattern = re.compile('(?i).*{}.*'.format(keyword))
            if (pattern.match(row['Body (HTML)']) or pattern.match(row['Title'])):
                #print('<Debug> Index: {} contains: {}'.format(index,tag))
                keyword_holder.append(tag)
                # Result found
        dataframe.loc[index,'Tags'] = ','.join(keyword_holder) # Appending the tag to his location index and cleaning the output.
    return dataframe

if __name__ == '__main__':
    setDataFrameOptions()
    dataframe = ReadCSV('products_export.csv')
    Experimental_AI_FEED = False

    if (Experimental_AI_FEED):
        dataframe = dataframe.applymap(str)
        keyworded_dataframe = dataframe.applymap(ExperimentalApply)
        keyworded_dataframe.to_csv(getcwd()+ '\\output\\output_keyworded_experimental.csv', sep=',', encoding='utf-8', index=False)
    else:
        dataframe = SetKeys(dataframe)
        #print("Number of total tags: ".format(dataframe["Tags"].value_counts()))
        #print("Number of total elements: ".format(dataframe["Handle"].value_counts()))
        dataframe.to_csv(getcwd()+ '\\output\\output_keyworded.csv', sep=',', encoding='utf-8', index=False)


"""
# Example usage:
# dataframe.loc[row_indexer, column_indexer] = value
# dataframe.loc[[0,3], 'Z'] = 3

# Write to CSV
# df.to_csv(file_name, sep=',', encoding='utf-8', index=False)
"""